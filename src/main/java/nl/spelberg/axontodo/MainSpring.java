package nl.spelberg.axontodo;

import java.util.UUID;
import nl.spelberg.axontodo.domain.CreateToDoItemCommand;
import nl.spelberg.axontodo.domain.MarkCompletedCommand;
import org.axonframework.commandhandling.gateway.CommandGateway;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class MainSpring {

    private CommandGateway commandGateway;

    public MainSpring(CommandGateway commandGateway) {
        this.commandGateway = commandGateway;
    }

    public static void main(String[] args) {
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("META-INF/axon-config.xml");
        MainSpring runner = new MainSpring(applicationContext.getBean(CommandGateway.class));
        runner.run();
    }

    private void run() {
        final String itemId = UUID.randomUUID().toString();
        commandGateway.send(new CreateToDoItemCommand(itemId, "Need to do this"));
        commandGateway.send(new MarkCompletedCommand(itemId));

        System.out.println("-->  Done publishing commands, now wait 2 seconds.\n" +
                "     Check that all events are already handled (commandhandler and eventhandler are called in a single thread)");
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
        System.out.println("-->  Done.");
    }

}
