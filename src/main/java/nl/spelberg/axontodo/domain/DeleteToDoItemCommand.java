package nl.spelberg.axontodo.domain;

import javax.xml.bind.annotation.XmlRootElement;
import nl.spelberg.commons.utils.ui.AbstractJsonObject;
import org.axonframework.commandhandling.annotation.TargetAggregateIdentifier;

@XmlRootElement
public class DeleteToDoItemCommand extends AbstractJsonObject {

    @TargetAggregateIdentifier
    private String toDoId;

    /**
     * Json Constructor.
     */
    @SuppressWarnings("UnusedDeclaration")
    protected DeleteToDoItemCommand() {
    }

    public DeleteToDoItemCommand(String toDoId) {
        if (toDoId == null) {
            throw new IllegalArgumentException("toDoId is null");
        }
        this.toDoId = toDoId;
    }

}
