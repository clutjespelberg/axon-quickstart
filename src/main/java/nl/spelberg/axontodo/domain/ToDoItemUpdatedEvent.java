package nl.spelberg.axontodo.domain;

public class ToDoItemUpdatedEvent {

    private final String toDoId;
    private final String updatedDescription;

    public ToDoItemUpdatedEvent(String toDoId, String updatedDescription) {
        this.toDoId = toDoId;
        this.updatedDescription = updatedDescription;
    }

    public String getToDoId() {
        return toDoId;
    }

    public String getUpdatedDescription() {
        return updatedDescription;
    }
}
