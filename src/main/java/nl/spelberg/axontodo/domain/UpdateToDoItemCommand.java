package nl.spelberg.axontodo.domain;

import org.apache.commons.lang3.StringUtils;
import org.axonframework.commandhandling.annotation.TargetAggregateIdentifier;

public class UpdateToDoItemCommand {

    @TargetAggregateIdentifier
    private final String toDoId;

    private final String updatedDescription;

    public UpdateToDoItemCommand(String toDoId, String updatedDescription) {
        if (toDoId == null) {
            throw new IllegalArgumentException("toDoId is null");
        }
        if (StringUtils.isBlank(updatedDescription)) {
            throw new IllegalArgumentException("updatedDescription is null or blank");
        }
        this.toDoId = toDoId;
        this.updatedDescription = updatedDescription;
    }

    public String getUpdatedDescription() {
        return updatedDescription;
    }
}
