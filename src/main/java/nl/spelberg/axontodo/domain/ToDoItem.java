package nl.spelberg.axontodo.domain;

import nl.spelberg.commons.utils.test.ThreadUtils;
import org.axonframework.commandhandling.annotation.CommandHandler;
import org.axonframework.eventhandling.annotation.EventHandler;
import org.axonframework.eventsourcing.annotation.AbstractAnnotatedAggregateRoot;
import org.axonframework.eventsourcing.annotation.AggregateIdentifier;

/**
 * Gemaakt op basis van de Axon quickstart guide op
 * <a href="http://www.axonframework.org/axon-2-quickstart-guide/">http://www.axonframework.org/axon-2-quickstart-guide/</a>
 */
public class ToDoItem extends AbstractAnnotatedAggregateRoot<String> {

    @AggregateIdentifier
    private String toDoId;
    private boolean markedComplete;

    /**
     * Axon constructor.
     */
    @SuppressWarnings("UnusedDeclaration")
    protected ToDoItem() {
        System.out.println("CREATE empty ToDoItem AggregateRoot");
    }

    @CommandHandler
    public ToDoItem(CreateToDoItemCommand command) {
        System.out.println("CREATE CommandHandler CreateToDoItem: " + command.getToDoId() + " '" + command.getDescription() + "'");
        ThreadUtils.sleep(1000);
        apply(new ToDoItemCreatedEvent(command.getToDoId(), command.getDescription()));
        System.out.println("  DONE CommandHandler CreateToDoItem: " + command.getToDoId() + " '" + command.getDescription() + "'");
    }

    @CommandHandler
    public void update(UpdateToDoItemCommand command) {
        System.out.println("HANDLE CommandHandler Update: " + toDoId + " '" + command.getUpdatedDescription() + "'");

        if (markedComplete) {
            throw new IllegalStateException("Trying to update a TodoItem when item is marked as complete (" + toDoId + ")");
        }

        apply(new ToDoItemUpdatedEvent(toDoId, command.getUpdatedDescription()));
        System.out.println("  DONE CommandHandler Update: " + toDoId + " '" + command.getUpdatedDescription() + "'");
    }

    @CommandHandler
    public void markCompleted(MarkCompletedCommand command) {
        System.out.println("HANDLE CommandHandler MarkCompleted: " + toDoId);

        if(markedComplete) {
            throw new IllegalStateException("Trying to mark todoItem as marked when it is already marked ("+toDoId+")");
        }

        apply(new ToDoItemCompletedEvent(toDoId));
        System.out.println("  DONE CommandHandler MarkCompleted: " + toDoId);
    }

    @CommandHandler
    public void delete(DeleteToDoItemCommand command) {
        System.out.println("HANDLE DeleteToDoItem: " + toDoId);
        ThreadUtils.sleep(1000);
        apply(new ToDoItemDeletedEvent(toDoId));
        System.out.println("  DONE DeleteToDoItem: " + toDoId + " DONE");
    }

    @EventHandler
    public void on(ToDoItemCreatedEvent event) {
        System.out.println(" DOMAINEVENTHANDLER CreatedToDoItem: " + event.getToDoId() + " '" + event.getDescription() + "'");
        this.toDoId = event.getToDoId();
        this.markedComplete = false;
    }

    @EventHandler
    public void on(ToDoItemUpdatedEvent event) {
        System.out.println(" DOMAINEVENTHANDLER Updated: " + toDoId + " '" + event.getUpdatedDescription() + "'");
    }

    @EventHandler
    public void on(ToDoItemCompletedEvent event) {
        System.out.println(" DOMAINEVENTHANDLER MarkCompleted: " + toDoId);
        this.markedComplete = true;
    }

    @EventHandler
    public void on(ToDoItemDeletedEvent event) {
        System.out.println(" DOMAINEVENTHANDLER Deleted: " + toDoId);
        markDeleted();
    }

}
