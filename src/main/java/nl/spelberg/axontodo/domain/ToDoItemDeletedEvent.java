package nl.spelberg.axontodo.domain;

public class ToDoItemDeletedEvent {

    private final String toDoId;

    public ToDoItemDeletedEvent(String toDoId) {
        this.toDoId = toDoId;
    }

    public String getToDoId() {
        return toDoId;
    }
}
