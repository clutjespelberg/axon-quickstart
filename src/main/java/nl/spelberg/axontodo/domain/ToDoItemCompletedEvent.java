package nl.spelberg.axontodo.domain;

public class ToDoItemCompletedEvent {

    private final String toDoId;

    public ToDoItemCompletedEvent(String toDoId) {
        this.toDoId = toDoId;
    }

    public String getToDoId() {
        return toDoId;
    }
}
