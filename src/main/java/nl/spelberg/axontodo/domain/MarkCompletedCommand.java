package nl.spelberg.axontodo.domain;

import javax.xml.bind.annotation.XmlRootElement;
import nl.spelberg.commons.utils.ui.AbstractJsonObject;
import org.axonframework.commandhandling.annotation.TargetAggregateIdentifier;

@XmlRootElement
public class MarkCompletedCommand extends AbstractJsonObject {

    @TargetAggregateIdentifier
    private String toDoId;

    /**
     * Json Constructor.
     */
    @SuppressWarnings("UnusedDeclaration")
    protected MarkCompletedCommand() {
    }

    public MarkCompletedCommand(String toDoId) {
        if (toDoId == null) {
            throw new IllegalArgumentException("toDoId is null");
        }
        this.toDoId = toDoId;
    }
}
