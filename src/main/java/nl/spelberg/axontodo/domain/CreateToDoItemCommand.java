package nl.spelberg.axontodo.domain;

import org.apache.commons.lang3.StringUtils;
import org.axonframework.commandhandling.annotation.TargetAggregateIdentifier;

public class CreateToDoItemCommand {

    @TargetAggregateIdentifier
    private final String toDoId;
    private final String description;

    public CreateToDoItemCommand(String toDoId, String description) {
        if (toDoId == null) {
            throw new IllegalArgumentException("toDoId is null");
        }
        if (StringUtils.isBlank(description)) {
            throw new IllegalArgumentException("description is null or blank");
        }
        this.toDoId = toDoId;
        this.description = description;
    }

    public String getToDoId() {
        return toDoId;
    }

    public String getDescription() {
        return description;
    }
}
