package nl.spelberg.axontodo.domain;

public class ToDoItemCreatedEvent {

    private final String toDoId;
    private final String description;

    public ToDoItemCreatedEvent(String toDoId, String description) {
        this.toDoId = toDoId;
        this.description = description;
    }

    public String getToDoId() {
        return toDoId;
    }

    public String getDescription() {
        return description;
    }
}
