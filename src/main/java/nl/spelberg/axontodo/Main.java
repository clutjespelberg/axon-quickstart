package nl.spelberg.axontodo;

import java.io.File;
import java.util.UUID;
import nl.spelberg.axontodo.domain.CreateToDoItemCommand;
import nl.spelberg.axontodo.domain.DeleteToDoItemCommand;
import nl.spelberg.axontodo.domain.MarkCompletedCommand;
import nl.spelberg.axontodo.domain.ToDoItem;
import nl.spelberg.axontodo.domain.UpdateToDoItemCommand;
import nl.spelberg.axontodo.query.LoggingEventHandler;
import org.axonframework.commandhandling.CommandBus;
import org.axonframework.commandhandling.SimpleCommandBus;
import org.axonframework.commandhandling.annotation.AggregateAnnotationCommandHandler;
import org.axonframework.commandhandling.gateway.CommandGateway;
import org.axonframework.commandhandling.gateway.DefaultCommandGateway;
import org.axonframework.eventhandling.EventBus;
import org.axonframework.eventhandling.SimpleEventBus;
import org.axonframework.eventhandling.annotation.AnnotationEventListenerAdapter;
import org.axonframework.eventsourcing.EventSourcingRepository;
import org.axonframework.eventstore.EventStore;
import org.axonframework.eventstore.fs.FileSystemEventStore;
import org.axonframework.eventstore.fs.SimpleEventFileResolver;

public class Main {

    public static void main(String[] args) {
        // let's start with the Command Bus
        CommandBus commandBus = new SimpleCommandBus();

        // the CommandGateway provides a friendlier API
        CommandGateway commandGateway = new DefaultCommandGateway(commandBus);

        // we'll store Events on the FileSystem, in the "events/" folder
        EventStore eventStore = new FileSystemEventStore(new SimpleEventFileResolver(new File("./target/events")));

        // a Simple Event Bus will do
        EventBus eventBus = new SimpleEventBus();

        // we need to configure the repository
        EventSourcingRepository<ToDoItem> repository = new EventSourcingRepository<ToDoItem>(ToDoItem.class);
        repository.setEventStore(eventStore);
        repository.setEventBus(eventBus);

        // Axon needs to know that our ToDoItem Aggregate can handle commands
        AggregateAnnotationCommandHandler.subscribe(ToDoItem.class, repository, commandBus);

        // Hook up eventlisteners
        AnnotationEventListenerAdapter.subscribe(new LoggingEventHandler(), eventBus);

        // and let's send some Commands on the CommandBus.
        final String itemId = UUID.randomUUID().toString();

        System.out.println();
        System.out.println("SENDING COMMAND: CreateToDoItem('" + itemId + "')");
        commandGateway.send(new CreateToDoItemCommand(itemId, "Need to do this"));

        System.out.println();
        System.out.println("SENDING COMMAND: Update('" + itemId + "')");
        commandGateway.send(new UpdateToDoItemCommand(itemId, "Really need to do this!"));

        System.out.println();
        System.out.println("SENDING COMMAND: MarkCompleted('" + itemId + "')");
        commandGateway.send(new MarkCompletedCommand(itemId));

        System.out.println();
        System.out.println("SENDING COMMAND: DeleteToDoItem('" + itemId + "')");
        commandGateway.send(new DeleteToDoItemCommand(itemId));

        System.out.println();
        System.out.println("-->  Done publishing commands, now wait 2 seconds.\n" +
                "     Check that all events are already handled (commandhandler and eventhandler are called in a single thread)");
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
        System.out.println("-->  Done.");

    }
}
