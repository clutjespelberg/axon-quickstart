package nl.spelberg.axontodo.uiservice;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import nl.spelberg.axontodo.domain.CreateToDoItemCommand;
import nl.spelberg.axontodo.domain.DeleteToDoItemCommand;
import nl.spelberg.axontodo.domain.MarkCompletedCommand;
import nl.spelberg.axontodo.query.ToDoItemAttributes;
import nl.spelberg.axontodo.query.ToDoList;
import nl.spelberg.axontodo.query.ToDoQueryModel;
import org.axonframework.commandhandling.gateway.CommandGateway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Path("/todo/")
public class UiService {

    @Autowired
    private CommandGateway commandGateway;

    @Autowired
    private ToDoQueryModel toDoQueryModel;

    @GET
    @Path("list")
    public ToDoList getToDoItems() {
        return toDoQueryModel.listToDoItems();
    }

    @POST
    @Path("create")
    public String create(CreateToDoItemJson createToDoItemJson) {
        CreateToDoItemCommand createToDoItemCommand = createToDoItemJson.asCreateCommand();
        commandGateway.send(createToDoItemCommand);
        return createToDoItemCommand.getToDoId();
    }

    @POST
    @Path("markcomplete")
    public void markComplete(MarkCompletedCommand command) {
        commandGateway.send(command);
    }

    @POST
    @Path("delete")
    public void delete(DeleteToDoItemCommand command) {
        commandGateway.send(command);
    }

    @POST
    @Path("deletecompleted")
    public void deleteCompleted() {
        ToDoList toDoList = toDoQueryModel.listCompletedToDoItems();
        for (ToDoItemAttributes toDoItemAttributes : toDoList.getToDoItems()) {
            commandGateway.send(new DeleteToDoItemCommand(toDoItemAttributes.getToDoId()));
        }
    }

}
