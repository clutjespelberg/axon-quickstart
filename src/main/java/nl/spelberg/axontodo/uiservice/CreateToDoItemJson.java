package nl.spelberg.axontodo.uiservice;

import java.util.UUID;
import javax.xml.bind.annotation.XmlRootElement;
import nl.spelberg.axontodo.domain.CreateToDoItemCommand;
import nl.spelberg.commons.utils.ui.AbstractJsonObject;
import org.springframework.util.Assert;

@XmlRootElement
public class CreateToDoItemJson extends AbstractJsonObject {

    private String description;

    /**
     * JSON constructor
     */
    @Deprecated
    protected CreateToDoItemJson() {
    }

    public CreateToDoItemJson(String description) {
        Assert.notNull(description, "description is null");
        this.description = description;
    }

    public CreateToDoItemCommand asCreateCommand() {
        String toDoId = UUID.randomUUID().toString();
        return new CreateToDoItemCommand(toDoId, description);
    }

}
