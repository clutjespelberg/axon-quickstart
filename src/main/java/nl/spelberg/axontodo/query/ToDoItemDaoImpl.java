package nl.spelberg.axontodo.query;

import java.util.List;
import nl.spelberg.commons.utils.dao.AbstractJpaDao;
import org.springframework.stereotype.Repository;

@Repository
public class ToDoItemDaoImpl extends AbstractJpaDao<ToDoItemEntity, String> implements ToDoItemDao {

    @Override
    public List<ToDoItemEntity> list() {
        return entityManager.createNamedQuery("ToDoItem.list", ToDoItemEntity.class).getResultList();
    }

    @Override
    public List<ToDoItemEntity> listCompleted() {
        return entityManager.createNamedQuery("ToDoItem.listCompleted", ToDoItemEntity.class).getResultList();
    }

}
