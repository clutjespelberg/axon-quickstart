package nl.spelberg.axontodo.query;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

@XmlAccessorType(XmlAccessType.FIELD)
public class ToDoItemAttributes implements Serializable {

    private String toDoId;
    private String toDoDescription;
    private boolean markedComplete;


    /**
     * JSON constructor
     */
    public ToDoItemAttributes() {
    }

    public ToDoItemAttributes(String toDoId, String toDoDescription, boolean markedComplete) {
        this.toDoId = toDoId;
        this.toDoDescription = toDoDescription;
        this.markedComplete = markedComplete;
    }

    public String getToDoId() {
        return toDoId;
    }
}
