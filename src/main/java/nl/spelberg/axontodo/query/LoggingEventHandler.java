package nl.spelberg.axontodo.query;

import nl.spelberg.axontodo.domain.ToDoItemCompletedEvent;
import nl.spelberg.axontodo.domain.ToDoItemCreatedEvent;
import nl.spelberg.axontodo.domain.ToDoItemDeletedEvent;
import nl.spelberg.axontodo.domain.ToDoItemUpdatedEvent;
import org.axonframework.eventhandling.annotation.EventHandler;

public class LoggingEventHandler {

    @EventHandler
    public void handle(ToDoItemCreatedEvent event) {
        System.out.println(" QUERY EVENT HANDLED: We've got something to do: " + event.getDescription() + " (" + event.getToDoId() + ")");
    }

    @EventHandler
    public void handle(ToDoItemUpdatedEvent event) {
        System.out.println(" QUERY EVENT HANDLED: Updated something to do: " + event.getUpdatedDescription() + " (" + event.getToDoId() + ")");
    }

    @EventHandler
    public void handle(ToDoItemCompletedEvent event) {
        System.out.println(" QUERY EVENT HANDLED: We've completed a task: " + event.getToDoId());
    }

    @EventHandler
    public void handle(ToDoItemDeletedEvent event) {
        System.out.println(" QUERY EVENT HANDLED: We've deleted a task: " + event.getToDoId());
    }
}
