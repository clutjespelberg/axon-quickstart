package nl.spelberg.axontodo.query;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import nl.spelberg.commons.utils.dao.AbstractQueryEntity;
import org.springframework.util.Assert;

@Entity
@NamedQueries({
        @NamedQuery(name = "ToDoItem.list", query = "from ToDoItemEntity order by toDoDescriptionUpper"),
        @NamedQuery(name = "ToDoItem.listCompleted", query = "from ToDoItemEntity where completed = true order by toDoDescriptionUpper")
})
public class ToDoItemEntity extends AbstractQueryEntity {

    @Id
    private String toDoId;

    @Basic
    private String toDoDescription;

    @Basic
    private String toDoDescriptionUpper;

    @Basic
    private boolean completed;

    /**
     * JPA constructor
     */
    @Deprecated
    protected ToDoItemEntity() {
    }

    public ToDoItemEntity(String toDoId, String toDoDescription) {
        Assert.notNull(toDoId, "toDoId is null");
        Assert.notNull(toDoDescription, "toDoDescription is null");
        this.toDoId = toDoId;
        this.toDoDescription = toDoDescription;
        this.toDoDescriptionUpper = toDoDescription.toUpperCase();
        this.completed = false;
    }

    public void markCompleted() {
        this.completed = true;
    }

    public ToDoItemAttributes asToDoItemAttributes() {
        return new ToDoItemAttributes(toDoId, toDoDescription, completed);
    }
}
