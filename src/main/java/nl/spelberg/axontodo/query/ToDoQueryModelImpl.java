package nl.spelberg.axontodo.query;

import java.util.ArrayList;
import java.util.List;
import nl.spelberg.axontodo.domain.ToDoItemCompletedEvent;
import nl.spelberg.axontodo.domain.ToDoItemCreatedEvent;
import nl.spelberg.axontodo.domain.ToDoItemDeletedEvent;
import org.axonframework.eventhandling.annotation.EventHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
class ToDoQueryModelImpl implements ToDoQueryModel {

    @Autowired
    private ToDoItemDao toDoItemDao;

    @EventHandler
    public void handle(ToDoItemCreatedEvent event) {
        ToDoItemEntity toDoItemEntity = new ToDoItemEntity(event.getToDoId(), event.getDescription());
        toDoItemDao.persist(toDoItemEntity);
    }

    @EventHandler
    public void handle(ToDoItemCompletedEvent event) {
        ToDoItemEntity toDoItemEntity = toDoItemDao.get(event.getToDoId());
        toDoItemEntity.markCompleted();
    }

    @EventHandler
    public void handle(ToDoItemDeletedEvent event) {
        toDoItemDao.deleteById(event.getToDoId());
    }

    @Override
    public ToDoItemAttributes get(String toDoId) {
        return toDoItemDao.get(toDoId).asToDoItemAttributes();
    }

    @Override
    public ToDoList listToDoItems() {
        List<ToDoItemEntity> all = toDoItemDao.list();

        List<ToDoItemAttributes> toDoItems = new ArrayList<ToDoItemAttributes>();
        for (ToDoItemEntity toDoItemEntity : all) {
            toDoItems.add(toDoItemEntity.asToDoItemAttributes());
        }
        return new ToDoList(toDoItems);
    }

    @Override
    public ToDoList listCompletedToDoItems() {
        List<ToDoItemEntity> all = toDoItemDao.listCompleted();

        List<ToDoItemAttributes> toDoItems = new ArrayList<ToDoItemAttributes>();
        for (ToDoItemEntity toDoItemEntity : all) {
            toDoItems.add(toDoItemEntity.asToDoItemAttributes());
        }
        return new ToDoList(toDoItems);
    }


}