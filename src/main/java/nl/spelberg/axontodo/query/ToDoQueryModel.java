package nl.spelberg.axontodo.query;

public interface ToDoQueryModel {

    ToDoItemAttributes get(String toDoId);

    ToDoList listToDoItems();

    ToDoList listCompletedToDoItems();
}
