package nl.spelberg.axontodo.query;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlRootElement;
import nl.spelberg.commons.utils.ui.AbstractJsonObject;

@XmlRootElement
public class ToDoList extends AbstractJsonObject {

    private List<ToDoItemAttributes> toDoItems;

    /**
     * JSON constructor
     */
    @Deprecated
    protected ToDoList() {
        this.toDoItems = new ArrayList<ToDoItemAttributes>();
    }

    public ToDoList(List<ToDoItemAttributes> toDoItems) {
        this.toDoItems = toDoItems;
    }

    public List<ToDoItemAttributes> getToDoItems() {
        return toDoItems;
    }
}
