package nl.spelberg.axontodo.query;

import java.util.List;
import nl.spelberg.commons.utils.dao.JpaDaoInterface;

public interface ToDoItemDao extends JpaDaoInterface<ToDoItemEntity, String> {

    List<ToDoItemEntity> list();

    List<ToDoItemEntity> listCompleted();
}
