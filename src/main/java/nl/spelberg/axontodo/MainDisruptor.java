package nl.spelberg.axontodo;

import java.io.File;
import java.util.UUID;
import nl.spelberg.axontodo.domain.CreateToDoItemCommand;
import nl.spelberg.axontodo.domain.MarkCompletedCommand;
import nl.spelberg.axontodo.domain.ToDoItem;
import nl.spelberg.axontodo.query.LoggingEventHandler;
import org.axonframework.commandhandling.annotation.AggregateAnnotationCommandHandler;
import org.axonframework.commandhandling.disruptor.DisruptorCommandBus;
import org.axonframework.commandhandling.gateway.CommandGateway;
import org.axonframework.commandhandling.gateway.DefaultCommandGateway;
import org.axonframework.eventhandling.ClusteringEventBus;
import org.axonframework.eventhandling.annotation.AnnotationEventListenerAdapter;
import org.axonframework.eventsourcing.GenericAggregateFactory;
import org.axonframework.eventstore.EventStore;
import org.axonframework.eventstore.fs.FileSystemEventStore;
import org.axonframework.eventstore.fs.SimpleEventFileResolver;
import org.axonframework.repository.Repository;

public class MainDisruptor {

    public static void main(String[] args) {
        System.out.println("Starting...");

        // we'll store Events on the FileSystem, in the "events/" folder
        EventStore eventStore = new FileSystemEventStore(new SimpleEventFileResolver(new File("./target/events")));

        // a Simple Event Bus will do
        ClusteringEventBus eventBus = new ClusteringEventBus();

        // let's start with the Command Bus
        DisruptorCommandBus commandBus = new DisruptorCommandBus(eventStore, eventBus);

        // we need to configure the repository
        Repository<ToDoItem> repository = commandBus.createRepository(new GenericAggregateFactory<ToDoItem>(ToDoItem.class));

        // Hook up eventlisteners
        AnnotationEventListenerAdapter.subscribe(new LoggingEventHandler(), eventBus);


        // the CommandGateway provides a friendlier API
        CommandGateway commandGateway = new DefaultCommandGateway(commandBus);

        // Axon needs to know that our ToDoItem Aggregate can handle commands
        AggregateAnnotationCommandHandler.subscribe(ToDoItem.class, repository, commandBus);

        // and let's send some Commands on the CommandBus.
        final String itemId = UUID.randomUUID().toString();
        commandGateway.send(new CreateToDoItemCommand(itemId, "Need to do this"));
        commandGateway.send(new MarkCompletedCommand(itemId));

        System.out.println("-->  Done publishing commands, now wait 2 seconds to let all eventhandlers finish\n" +
                "     Events are handled in a separate thread, so this message will be shown before events are handled.");
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
        System.out.println("-->  Stopping DisruptorCommandBus...");
        commandBus.stop();

        System.out.println("-->  Done.");
    }
}
