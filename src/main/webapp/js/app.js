angular.module('todolist', []).
    config(['$routeProvider', function($routeProvider){
        $routeProvider.
            when('/todolist', {templateUrl: 'partials/todo-list.html', controller: ToDoListCtrl}).
            when('/todoitem/:toDoId', {templateUrl: 'partials/todo-item.html', controller: ToDoItemCtrl}).
            otherwise({redirectTo: '/todolist'});
    }]);
