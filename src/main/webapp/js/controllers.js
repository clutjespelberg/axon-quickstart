function ToDoListCtrl($scope, $http) {

    $scope.toDoItems = [];
    $scope.newToDo = {
        "description": ""
    };
    $scope.spinner = {
        "show" : false,
        "depth" : 0
    };

    function showSpinner() {
        $scope.spinner.show = true;
        $scope.spinner.depth += 1;
    }

    function hideSpinner() {
        $scope.spinner.depth -= 1;
        $scope.spinner.show = $scope.spinner.depth > 0;
    }

    $scope.reload = function () {
        showSpinner();
        $http.get('api/rest/todo/list.json').success(function (data) {
            $scope.toDoItems = data.toDoItems;
            $scope.addToDoDescription = '';
            hideSpinner();
        });
    };

    $scope.markCompleted = function(toDoId) {
        var markCompletedCommand = {
            "toDoId": toDoId
        };
        showSpinner();
        $http.post('api/rest/todo/markcomplete', markCompletedCommand).success(function (data) {
            $scope.reload();
            hideSpinner();
        });
    };

    $scope.removeCompletedItems = function() {
        var currentToDoItems = $scope.toDoItems;
        for (var i = 0; i < currentToDoItems.length; i++) {
            var toDoItem = currentToDoItems[i];
            if (toDoItem.markedComplete) {
                var deleteCommand = {
                    "toDoId": toDoItem.toDoId
                };
                showSpinner();
                $http.post('api/rest/todo/delete', deleteCommand).success(function (data) {
                    $scope.reload();
                    hideSpinner();
                });
            }
        }
    };

    $scope.addToDoItem = function () {
        showSpinner();
        $http.post('api/rest/todo/create', $scope.newToDo).success(function (data) {
            $scope.reload();
            hideSpinner();
        });
        $scope.newToDo.description = "";
    };

    $scope.reload();

}

function ToDoItemCtrl($scope, $routeParams) {
    $scope.toDoId = $routeParams.toDoId;
}