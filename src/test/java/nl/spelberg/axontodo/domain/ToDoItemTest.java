package nl.spelberg.axontodo.domain;

import org.axonframework.eventsourcing.AggregateDeletedException;
import org.axonframework.test.FixtureConfiguration;
import org.axonframework.test.Fixtures;
import org.junit.Before;
import org.junit.Test;

public class ToDoItemTest {

    private FixtureConfiguration fixture;

    @Before
    public void setUp() {
        fixture = Fixtures.newGivenWhenThenFixture(ToDoItem.class);
    }

    @Test
    public void testCreateToDoItem() {
        fixture.given()
                .when(new CreateToDoItemCommand("todo1", "need to implement the aggregate"))
                .expectEvents(new ToDoItemCreatedEvent("todo1", "need to implement the aggregate"));
    }

    @Test
    public void testUpdateDescription() {
        fixture.given(new ToDoItemCreatedEvent("todo1", "need to implement the aggregate"))
                .when(new UpdateToDoItemCommand("todo1", "Really need to implement the aggregate"))
                .expectEvents(new ToDoItemUpdatedEvent("todo1", "Really need to implement the aggregate"));
    }

    @Test
    public void testMarkToDoItemCompleted() {
        fixture.given(new ToDoItemCreatedEvent("todo1", "need to implement the aggregate"))
                .when(new MarkCompletedCommand("todo1"))
                .expectEvents(new ToDoItemCompletedEvent("todo1"));
    }

    @Test
    public void testUpdateAfterMarkToDoItemCompleted() {
        fixture.given(new ToDoItemCreatedEvent("todo1", "need to implement the aggregate"), new ToDoItemCompletedEvent("todo1"))
                .when(new UpdateToDoItemCommand("todo1", "Really need to implement the aggregate"))
                .expectException(IllegalStateException.class);
    }

    @Test
    public void testMarkCompleteTwice() {
        fixture.given(new ToDoItemCreatedEvent("todo1", "need to implment the aggregate"), new ToDoItemCompletedEvent("todo1"))
                .when(new MarkCompletedCommand("todo1"))
                .expectException(IllegalStateException.class);
    }

    @Test
    public void testDeleteToDoItem() {
        fixture.given(new ToDoItemCreatedEvent("todo1", "need to implement the aggregate"))
                .when(new DeleteToDoItemCommand("todo1"))
                .expectEvents(new ToDoItemDeletedEvent("todo1"));
    }

    @Test
    public void testDeleteToDoItemTwice() {
        fixture.given(new ToDoItemCreatedEvent("todo1", "need to implement the aggregate"), new ToDoItemDeletedEvent("todo1"))
                .when(new DeleteToDoItemCommand("todo1"))
                .expectException(AggregateDeletedException.class);
    }


}
