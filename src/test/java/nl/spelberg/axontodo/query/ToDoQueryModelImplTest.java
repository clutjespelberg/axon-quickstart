package nl.spelberg.axontodo.query;

import nl.spelberg.axontodo.domain.ToDoItemCompletedEvent;
import nl.spelberg.axontodo.domain.ToDoItemCreatedEvent;
import nl.spelberg.axontodo.domain.ToDoItemDeletedEvent;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Answers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class ToDoQueryModelImplTest {

    @Mock(answer = Answers.RETURNS_SMART_NULLS)
    private ToDoItemDao toDoItemDao;

    @InjectMocks
    private ToDoQueryModelImpl eventHandler = new ToDoQueryModelImpl();

    private String toDoId;
    private String toDoDescription;

    @Before
    public void setUp() {

        toDoId = "todo1";
        toDoDescription = "implement todo eventhandling";
    }

    @After
    public void tearDown() {
        verifyNoMoreInteractions(toDoItemDao);
    }

    @Test
    public void testHandleToDoItemCreatedEvent() {

        // execute
        eventHandler.handle(new ToDoItemCreatedEvent(toDoId, toDoDescription));

        // verify
        verify(toDoItemDao).persist(new ToDoItemEntity(toDoId, toDoDescription));
    }

    @Test
    public void testHandleToDoItemCompletedEvent() {
        // setup
        ToDoItemEntity toDoItemEntity = new ToDoItemEntity(toDoId, toDoDescription);
        when(toDoItemDao.get(toDoId)).thenReturn(toDoItemEntity);

        // execute
        eventHandler.handle(new ToDoItemCompletedEvent(toDoId));

        // verify
        verify(toDoItemDao).get(toDoId);
        ToDoItemEntity expected = new ToDoItemEntity(toDoId, toDoDescription);
        expected.markCompleted();

        assertEquals(expected, toDoItemEntity);
    }

    @Test
    public void testHandleToDoItemDeletedEvent() {

        // execute
        eventHandler.handle(new ToDoItemDeletedEvent(toDoId));

        // verify
        verify(toDoItemDao).deleteById(toDoId);
    }
}
